## Repository for 中鋼人工智慧挑戰賽-字元辨識 competition


https://tbrain.trendmicro.com.tw/Competitions/Details/17


### Files

score.py to calculate the score


### Model

```python
x = MaxPooling2D(pool_size=(2, 2), padding='same')(x)
x = MaxPooling2D(pool_size=(2, 2), padding='same')(x)
x = Conv2D(filters=64, kernel_size=(7, 7), activation='relu')(x)
x = Conv2D_BN_Activation(filters=64, kernel_size=(7, 7))(x)
x = MaxPooling2D(pool_size=(3, 3), padding='same')(x)
x = Residual_Block(filters=64, kernel_size=(3, 3))(x)
x = Residual_Block(filters=64, kernel_size=(3, 3))(x)
x = Residual_Block(filters=64, kernel_size=(3, 3))(x)
x = MaxPooling2D(pool_size=(3, 3), padding='same')(x)
x = Dropout(0.2)(x)
x = Residual_Block(filters=128, kernel_size=(3, 3), with_conv_shortcut=True)(x)
x = Residual_Block(filters=128, kernel_size=(3, 3))(x)
x = Residual_Block(filters=128, kernel_size=(3, 3))(x)
x = MaxPooling2D(pool_size=(3, 3), padding='same')(x)
x = Dropout(0.2)(x)
x = Residual_Block(filters=256, kernel_size=(3, 3), with_conv_shortcut=True)(x)
x = Residual_Block(filters=256, kernel_size=(3, 3))(x)
x = Residual_Block(filters=256, kernel_size=(3, 3))(x)
x = MaxPooling2D(pool_size=(3, 3), padding='same')(x)
x = Dropout(0.3)(x)
x = Conv2D_BN_Activation(filters=256, kernel_size=(3, 3))(x)
x = MaxPooling2D(pool_size=(3, 3), padding='same')(x)
x = Dropout(0.3)(x)
x = Flatten()(x)
```
